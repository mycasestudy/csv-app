#!/bin/sh
CWD=$(pwd)
TARGET_DIR="${CWD}/artifacts"
echo "Building the image..."
docker run -v "${TARGET_DIR}":/target --rm -it $(docker build -q .)
echo "Image built, you will find the zipped Lambda function at ${TARGET_DIR}/build.zip"
