FROM python:3.9.7
RUN apt update && apt install -y zip
ADD src /src
ADD requirements.txt /requirements.txt
RUN pip install --target /tmp/package  -r /requirements.txt
CMD [ "bash", "-c", "[[ -e '/target/build.zip' ]] && rm /target/build.zip; cd /tmp/package && zip -q -r /target/build.zip . && zip -q -g -j /target/build.zip /src/*.py" ]
