#!/usr/bin/env python3
import os
import csv
import json
import boto3
import logging


log_level = logging.getLevelName(os.environ.get('LOG_LEVEL', 'INFO').upper())
logger = logging.getLogger()
logger.setLevel(log_level)
for module_name in logging.Logger.manager.loggerDict.keys():
    logging.getLogger(module_name).setLevel(log_level)

region = str(os.environ['REGION'])
table_name = str(os.environ['TABLE'])


def lambda_handler(event, context):
    s3 = boto3.client('s3')
    db = boto3.client('dynamodb', region_name=region)

    # Get the uploaded file details
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_name = event['Records'][0]['s3']['object']['key']
    logger.info(f'Uploaded file name: {file_name}')

    # Fetch the data from uploaded file
    csv_file = s3.get_object(Bucket=bucket_name, Key=file_name)
    data_set = csv_file['Body'].read().decode('utf-8').split('\n')
    csv_reader = csv.reader(data_set, delimiter=',', quotechar='"')
    rows = 0

    for row in csv_reader:
        rows += 1
        id = row[0]
        first_name = row[1]
        last_name = row[2]
        contact = row[3]

        # Insert record in DynamoDB table
        db.put_item(TableName=table_name,
                    Item={
                        'Id': {
                            'N': str(id)
                        },
                        'Firstname': {
                            'S': str(first_name)
                        },
                        'Lastname': {
                            'S': str(last_name)
                        },
                        'Contact': {
                            'N': str(contact)
                        },
                    })

    logger.info(f'Successfully added {rows} in customers table')

    return {
        'statusCode': 200,
        'body': json.dumps('Function execution completed!')
    }
