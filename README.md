# CSV Importing with Lambda Function
`src/main.py` contains a Python 3.9 AWS Lambda function source code which gets triggered on a CSV upload to S3 bucket and insert the records from CSV into DynamoDB.

CSV files should be like: `id,first_name,last_name,contact_no`, you can try following file `sample_data.csv`

### Prerequisites
To able to work on this project:
- If you want to add 3th party packages into requirements.txt you need [pip-tools](https://github.com/jazzband/pip-tools)
- The Docker installed. You may have to run `./build.sh` as root (using `sudo` or any other mechanism to do so), in case your user is not part of the `docker` group.

## Building the artifact
When you add 3th party dependencies into `requirements.in` you have to run following command `pip-compile requirements.in`. It will update the `requirements.txt` file.

To build the zipped function with all requirements (3th party packages), you need to run `./build.sh` and then you can find the zipped function located in `artifacts/build.zip`.

## Configuration
The following list of environment variables can be configured for the Lambda function:

| Variable name | Example value | Description |
|---|---|---|
| LOG_LEVEL | INFO | Default is "INFO". You can set python log level in lambda function by using this. |
| REGION | us-east-1 | AWS region of the DynamoDB. |
| TABLE | Customers | DynamoDB table name. |
